package SignalProcessing;

public interface SignalObserver {

	public void update();
}
