package SignalProcessing;

import java.util.ArrayList;
import java.util.List;

public class SignalSubject {

	private ArrayList <SignalObserver> list;
	
	public SignalSubject(){
		list = new ArrayList<SignalObserver>();
	}
	public void addObs(SignalObserver obs){
		list.add(obs);
	}
	
	public void removeObs(SignalObserver obs){
		list.remove(obs);
	}
	
	public void notifyObs(){
		for(int i = 0; i < list.size(); i++)
			((SignalObserver) list.toArray()[i]).update();
	}
}
