package com.example.audio_record;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import android.os.Environment;
import android.util.Log;

public class SignalProcessor {
    int IndexRed1 = 0;
	int IndexRed2 = 0;
	int troughIndexRed = 0;
	int peakIndexIR = 0;
	int period = 0;
	float offsum = 0;
	float  offavg = 0;
	float  redavg = 0;
	float redsum = 0;
	float threshPeakRed = 8800;
	float threshVallRed = -7150;
	float threshRed = 28000;
	int s = 0;
	int period1 = 0;
	int counter = 0;

	File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/pomessage.txt");	
	
	
	public SignalProcessor() throws FileNotFoundException{
	
		OutputStream os = new FileOutputStream(file);
		OutputStreamWriter bos = new OutputStreamWriter(os);
	}
	
	void processData(float d){
		if((IndexRed2 == 0) && (s>10000) && (d > threshPeakRed)){
			if(IndexRed1 == 0)
				IndexRed1 = s;
			else if((s-IndexRed1)>17)
				IndexRed2 = s;
			period1 = IndexRed2-IndexRed1;
		}
		
		if(period1 > 0){
			counter++;
			if((counter>9) && (counter<14)){
				redsum += d;
			}
			if(counter == 13){
				redavg = redsum/4;
			}
			if((counter > 15) && (counter < 20)){
				offsum += d;
			}
			if(counter == 19){
				offavg = offsum/4;
				Log.d("", "red = " + (redavg+offavg) + " r = "+ redavg + " o = " +offavg + " period  = " + period1 );
				String output = String.format(""+s +","+(redavg+offavg)+"\r\n");
				//bos.write(output);
				redsum = 0;
				offsum = 0;
			}
			if(counter == 23)
				counter = 0;
			s++;
		}
	}
}
